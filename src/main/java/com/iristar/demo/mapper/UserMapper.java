package com.iristar.demo.mapper;

import com.iristar.demo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
public interface UserMapper extends BaseMapper<User> {

}
