package com.iristar.demo.service;

import com.iristar.demo.entity.Dataset;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
public interface IDatasetService extends IService<Dataset> {

}
