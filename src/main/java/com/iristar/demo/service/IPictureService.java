package com.iristar.demo.service;

import com.iristar.demo.entity.Picture;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
public interface IPictureService extends IService<Picture> {

}
