package com.iristar.demo.service;

import com.iristar.demo.entity.Tagging;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
public interface ITaggingService extends IService<Tagging> {

}
