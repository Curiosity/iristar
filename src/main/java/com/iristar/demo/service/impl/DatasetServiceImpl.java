package com.iristar.demo.service.impl;

import com.iristar.demo.entity.Dataset;
import com.iristar.demo.mapper.DatasetMapper;
import com.iristar.demo.service.IDatasetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@Service
public class DatasetServiceImpl extends ServiceImpl<DatasetMapper, Dataset> implements IDatasetService {

}
