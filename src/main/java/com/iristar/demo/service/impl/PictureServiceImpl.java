package com.iristar.demo.service.impl;

import com.iristar.demo.entity.Picture;
import com.iristar.demo.mapper.PictureMapper;
import com.iristar.demo.service.IPictureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements IPictureService {

}
