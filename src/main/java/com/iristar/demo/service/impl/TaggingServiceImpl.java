package com.iristar.demo.service.impl;

import com.iristar.demo.entity.Tagging;
import com.iristar.demo.mapper.TaggingMapper;
import com.iristar.demo.service.ITaggingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@Service
public class TaggingServiceImpl extends ServiceImpl<TaggingMapper, Tagging> implements ITaggingService {

}
