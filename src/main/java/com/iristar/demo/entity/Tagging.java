package com.iristar.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Tagging implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tagging _id", type = IdType.AUTO)
    private Integer taggingId;

    private Integer pictureId;

    private String userId;

    /**
     * 操作时间
     */
    private LocalDate operationTime;

    /**
     * 0存在，1删除
     */
    private Integer status;

    @TableField("JSON")
    private String json;


}
