package com.iristar.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Dataset implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "dataset_id", type = IdType.AUTO)
    private Integer datasetId;

    private String datasetName;

    private String datasetPath;

    private LocalDate createAt;

    private String originalDatabase;

    private String pretreatmentMethod;

    /**
     * 0存在，1删除
     */
    private Integer status;


}
