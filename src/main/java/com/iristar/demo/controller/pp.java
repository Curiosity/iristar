package com.iristar.demo.controller;

import com.iristar.demo.common.lang.Result;
import com.iristar.demo.util.ShiroUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class pp {
    @RequiresAuthentication
    @PostMapping("/add")
    public Result add(){
        System.out.println("取出"+ShiroUtil.getProfile());
        return Result.succ(null);
    }
}
