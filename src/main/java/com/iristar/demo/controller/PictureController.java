package com.iristar.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@RestController
@RequestMapping("/demo/picture")
public class PictureController {

}
