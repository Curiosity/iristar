package com.iristar.demo.controller;


import com.iristar.demo.common.lang.Result;
import com.iristar.demo.entity.User;
import com.iristar.demo.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.CredentialNotFoundException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zqy
 * @since 2022-01-08
 */
@RestController
@RequestMapping("/demo/user")
public class UserController {

    @Autowired
    IUserService iUserService;

    //@RequiresAuthentication
    @RequestMapping("/index")
    public Object index(){
        User user = iUserService.getById(1);
        return Result.succ(user);
    }


    @PostMapping("/save")
    public Result save(@Validated @RequestBody User user){
        System.out.println("444444:"+ user.getUserName());
        return Result.succ(user);
    }
}
